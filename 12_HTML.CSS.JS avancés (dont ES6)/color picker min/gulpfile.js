const { src, dest, series, watch, parallel } = require("gulp");
const sassSync = require("gulp-sass");
const Sync = require("browser-sync");
const concat = require("gulp-useref");
const minJs = require("gulp-uglify");
const minCss = require("gulp-minify-css");
const or = require("gulp-if");
const supp = require("del");



function sass() {

    return src("app/scss/**/*.scss")
        .pipe(sassSync())
        .pipe(dest("app/css"))
        .pipe(Sync.reload({
            stream: true
        }))
};

function htmlLook() {
    return src("app/**/*.html")
        .pipe(Sync.reload({
            stream: true
        }))
}

function jsLook() {
    return src("app/js/**/*.js")
        .pipe(Sync.reload({
            stream: true
        }))
}


function browserSync() {
    Sync({
        server: {
            baseDir: "app"
        },
    })
}

function minJsCss() {
    return src("app/**/*.html")
        .pipe(concat())
        .pipe(dest("dist"))
}

function minifyJs() {
    return src("dist/js/*.js")
        .pipe(minJs())
        .pipe(dest("dist/js"))
}

function minifyCss() {
    return src("dist/css/*.css")
        .pipe(minCss())
        .pipe(dest("dist/css"))
}

function watcher() {

    watch("app/scss/**/*.scss", sass).on("change", function(event) {
        console.log(`fichier ${event} modifier`);
    })
    watch("app/*.html", htmlLook).on("change", function(event) {
        console.log(`fichier ${event.src} modifier`);
    })
    watch("app/js/*.js", jsLook).on("change", function(event) {
        console.log(`fichier ${event.src} modifier`);
    })
}

function transImg() {
    return src("app/img/**.*")
        .pipe(dest("dist/img"))
}

function transFont() {
    return src("app/fonts/**.*")
        .pipe(dest("dist/fonts"))
}

function suppDist() {
    supp("dist")
}


module.exports = {
    sass,
    browserSync,
    watcher,
    minJsCss,
    transImg,
    suppDist,
    watch: parallel(browserSync, watcher),
    dist: series(minJsCss, transImg, transFont, minifyJs, minifyCss)
}