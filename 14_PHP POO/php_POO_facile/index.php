<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

    <?php
class StrUtils{
    private $text;
    public function __construct($str){
        $this->text = $str;
    }
    public function bold(){
        echo "<p><B>$this->text</B></p>";
    }
    public function italic(){
        echo "<p><i>$this->text</i></p>";
    }
    public function underline(){
        echo "<p style='text-decoration: underline;'>$this->text</p>";
    }
    public function capitalize(){
        echo "<p style='text-transform: uppercase;'>$this->text</p>";
    }
    public function uglify(){
        echo "<p style='font-weight: bold;font-style: italic;text-decoration: underline;'>$this->text</p>";
    }

}
$myStr = new StrUtils('php is awesome !');

$myStr->bold();
$myStr->italic();
$myStr->underline();
$myStr->capitalize();
$myStr->uglify();


?>
</body>
</html>