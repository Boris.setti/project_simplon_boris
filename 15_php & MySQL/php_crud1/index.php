<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Crud</title>
</head>
<body>
<?php 
$bdd = new PDO('mysql:host=localhost;dbname=colyseum','root','');
echo "Exercice 1 Afficher tous les clients <br><br>";
foreach($bdd->query('SELECT * FROM clients;')as $client){
    echo "$client[1] $client[2]" ;
    echo "<br>";
};

echo "<br> Exercice 2 Afficher tous les types de spectacles possibles.<br><br>";

foreach($bdd->query('SELECT * FROM shows;')as $show){
    echo "$show[1]" ;
    echo "<br>";
};

echo "<br> Exercice 3 Afficher les 20 premiers clients.<br><br>";

$cli = $bdd->query('SELECT * FROM clients;');
for($i = 0 ; $i <20 ; $i++){
    $cliD = $cli->fetch();
    echo "$cliD[0] $cliD[1] $cliD[2]"; 
    echo "<br>";
};

echo "<br>Exercice 4 N'afficher que les clients possédant une carte de fidélité.<br><br>";

$cli = $bdd->query('SELECT lastName,firstName FROM clients WHERE cardNumber;');


foreach($cli as $cliD){
    echo "$cliD[0] $cliD[1] <br>";
};

echo "<br>Exercice 5 Afficher uniquement le nom et le prénom de tous les clients dont le nom commence par la lettre 'M'. Les afficher comme ceci : Nom : Nom du client Prénom : Prénom du client<br><br>";

$cli = $bdd->query('SELECT lastName,firstName FROM clients WHERE lastName LIKE "M%";');

// $test = $cli->fetch();
// echo $test;
foreach($cli as $cliD){
    echo "Nom : $cliD[0] Prénom : $cliD[1]<br>";
};

echo "<br>Exercice 6 Afficher le titre de tous les spectacles ainsi que l'artiste, la date et l'heure. Trier les titres par ordre alphabétique. Afficher les résultat comme ceci : Spectacle par artiste, le date à heure.<br><br>";

$cli = $bdd->query('SELECT title,performer,date,startTime,duration FROM shows ORDER BY title ASC;');

foreach($cli as $cliD){
    $timeD = $cliD[3] - $cliD[4];
    echo "Piece : $cliD[0]<br> Artiste : $cliD[1] <br> Date : $cliD[2] <br>Heure : $timeD H<br>";
    };


echo "<br>Exercice 7 Afficher tous les clients comme ceci : Nom : Nom de famille du client Prénom : Prénom du client Date de naissance : Date de naissance du client Carte de fidélité : Oui (Si le client en possède une) ou Non (s'il n'en possède pas) Numéro de carte : Numéro de la carte fidélité du client s'il en possède une.<br><br>";
  
$cli =$bdd->query('SELECT lastName,firstName,birthDate,card,cardNumber FROM clients;');

foreach($cli as $cliD){
    if($cliD[3] == 1){
        echo "Nom : $cliD[0] <br> Prenom : $cliD[1]<br> Date de naissance : $cliD[2] <br> Carte de fidelité : oui <br> Numéro de carte : $cliD[4]<br> <br>";
    }else{
    echo "Nom : $cliD[0] <br> Prenom : $cliD[1]<br> Date de naissance : $cliD[2] <br> Carte de fidelité : non <br><br>";
    }
};


?>
    
</body>
</html>
