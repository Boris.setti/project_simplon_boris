var express = require("express");
var bodyParser = require("body-parser");
const port = 8080;

var app = express();

const { Random } = require("random-js");
const random = new Random();
const value = random.integer(1, 10);
console.log("Chut... voici le nombre mystère : " + value);
console.log(__dirname)

app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get("/", function(req, res) {
    res.sendFile("index.html"); // ou sendFile
})

app.get("/nouvelle_chance_trop_haut/", function(req, res) {
    res.sendFile(__dirname + '/public/nouvelle_chance_trop_haut.html');
})
app.get("/nouvelle_chance_trop_bas/", function(req, res) {
    res.sendFile(__dirname + '/public/nouvelle_chance_trop_bas.html');
})


app.post("/reponse/", function(req, res) {
    console.log(`le chiffre ${req.body.chiffre}`)
    var reponse = req.body.chiffre;
    if (reponse == value) {
        console.log("Gagné !");
        res.send("Gagné");
    } else if (reponse > value) {
        console.log("Perdu trop haut!");
        res.redirect("/nouvelle_chance_trop_haut/");
    } else {
        console.log("Perdu trop bas!");
        res.redirect("/nouvelle_chance_trop_bas/");
    }
})


app.listen(port, () => console.log(`Je suis lancé ! ${port}`));