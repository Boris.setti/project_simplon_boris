<!-- Exercice 2 Sur la page index, faire un liens vers une autre page.
 Passer d'une page à l'autre le contenu des variables nom, prenom et age grâce aux sessions.
  Ces variables auront été définies directement dans le code.
Il faudra afficher le contenu de ces variables sur la deuxième page. -->

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

    <form action="/globalVariable/exercice2.php" method="post">
        Nom<input type="text" name="nom" id="nom" />
        Prenom<input type="text" name="prenom" id="prenom"/>
        Age<input type="number" name="age" id="age"/>
        
        <button type="submit">valider</button>
    </form>

    <?php

    setcookie('nom',$_POST["nom"],time()+3600);
    setcookie('prenom',$_POST["prenom"],time()+3600);
    setcookie('age',$_POST["age"],time()+3600);

    print_r($_COOKIE)

    
    
    ?>
    
</body>
</html>