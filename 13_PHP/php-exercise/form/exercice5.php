<!-- Exercice 5 Créer un formulaire sur la page index.php avec :

Une liste déroulante pour la civilité (Mr ou Mme)
Un champ texte pour le nom
Un champ texte pour le prénom
Ce formulaire doit rediriger vers la page index.php.
Vous avez le choix de la méthode. -->

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exo loop</title>
</head>
<body>
<h1>exo form n°5</h1>
    <form action="/form/exercice5.php" method="post">
        <select name="genre" id="genre">
            <option value="Mme">Mme</option>
            <option value="Mr">Mr</option>
        </select>
        <input type="text" name="nom" id="nom" value=''/>
        <input type="text" name="prenom" id="prenom" value=''/>
        
        <button type="submit">valider</button>
    </form>

<?php
if($_POST['genre']==true){
    echo $_POST['genre'] . '<br>'; 
 };

if($_POST['nom']==true){
    echo $_POST['nom'] . '<br>'; 
 };

 if($_POST['prenom'] == true){
     echo $_POST['prenom'].'<br>';
 };
 
?>
</body>
</html>