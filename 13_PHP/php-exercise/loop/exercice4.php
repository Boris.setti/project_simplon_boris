<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exo loop</title>
</head>
<body>
<h1>exo loop n°4</h1>

<?php

$var1 = 1;


for( $var1; $var1 <= 10 ; $var1++ ){
    $result= $var1 / 2;
    echo "<p>$var1 / 2 = $result</p>";
};

?>
</body>
</html>