
<!-- ##Exercice 10 Avec le tableau de l'exercice 5, afficher toutes les valeurs de ce tableau ainsi que les clés associés.
Cela pourra être, par exemple, de la forme : "Le département" + nom_departement + "a le numéro" + num_departement -->
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exo function</title>
</head>

<body>
    <h1>exo array n°10</h1>

    <?php

    $mois = array(
        02 => "Aisne",
        59 => "Nord",
        60 => "Oise",
        62 => "Pas-de-Calais",
        80 => "Somme"

    );
    for ($i=0; $i < count($mois); $i++) {
       $tab = array_values($mois);
       $valueTab = $tab[$i];
       $key = array_search($valueTab, $mois);
       echo "Le département $valueTab a le numéro $key <br>";
    };
    ?>
</body>

</html>