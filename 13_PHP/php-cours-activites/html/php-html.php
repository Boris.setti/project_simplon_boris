<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>PHP & HTML</title>
  </head>
  <body>
    <h1>Liste des élèves</h1>
    <!-- Instructions : Afficher la liste des éléves qui sont présent dans le tableau $students -->
    <?php
        //students
        $students = ['Hulk', 'Iron Man', 'Wonder Woman', 'Black Widow', 'Malicia'];
     ?>
     <ul>
       <?php 
       foreach($students as $superHero){
         echo "Élevé :$superHero <br>";
       }
       ?>
     </ul>
     <hr>
     <h1>Date du jour</h1>
     <form>

       <!-- Instructions : Créer la liste de jour (en chiffres), de mois (en chiffres) et d'année en PHP. -->
       <label for="day">Day</label>
       <select  name="day"><option>--</option><?php for($i = 1 ; $i<=31 ;$i++){echo "<option>$i</option>";}?></select>
       <label for="month">Month</label>
       <select  name="month"><option>--</option><?php for($i = 1 ; $i<=12 ;$i++){echo "<option>$i</option>";}?></select>
       <label for="year">Year</label>
       <select  name="year"><option>----</option><?php for($i = 2019 ; $i>=1900 ;$i--){echo "<option>$i</option>";}?></select>
     </form>
     <hr>
     <?php
      if($_GET["sex"] == garçon ){
        echo "<p>Je suis un garçon</p>";
      }elseif($_GET["sex"] == fille){
        echo "<p>Je suis une fille</p>";
      }else {
      echo "<p>Je suis indéfini</p>"; 
    }
     ?>
   
  </body>
</html>
