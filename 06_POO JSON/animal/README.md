# intro-poo


## Un bestiaire

Créer une application qui créée un animal à chaque clic du bouton. 
Cet objet doit être représenté par une simple boite HTML contenant le nom de l'animal.

- Ne rien modifier dans app.js
- Ne travailler que dans Animal.js


### Héritage

Créez des classes Carnivore, Herbivore et Végétal, qui réagissent aux boutons correspondants,
et héritent d'une classe Vivant, et qui respectent les caractéristiques suivantes :
- paramètres Vivant : date de naissance, espérance de vie
- méthodes Vivant : dormir(temps), mourir()
- méthodes Carnivore et Herbivore : bouger(), manger(victime)
- méthodes Végétal : pousser(hauteur)


### Version 3 - mourir, nourrir

Ajoutez un bouton "mourir" dans chaque boite. Ce bouton doit appeler une méthode
"mourir", qui supprime le HTML, et qui détruit à la fois l'instance de l'animal.

Ajouter un champ INPUT de type NUMBER, et un bouton "nourrir" dans chacunes des boites
Animal. Le clic sur le bouton "nourrir" va incrémenter la quantité de nourriture dans l'estomac
des bestioles. Il faudra donc ajouter un paramètre à votre classe Animal.


## Annuaire

Créer un annuaire des apprenants Simplon et des formateurs.