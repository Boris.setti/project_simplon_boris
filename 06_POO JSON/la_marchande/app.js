// Mes classes

function Produit(n,p){
    this.nom=n;
    this.prix=p;
}


function Panier(){
    this.totalHT= 0 ;
    this.totalTTC= 0 ;
    this.TVA = 1.055 ;
    this.ProduitsPanier =[]
    this.ajoute = function (p){

        this.ProduitsPanier.push(p.nom)
        this.totalHT = this.totalHT + p.prix;
        this.totalTTC = this.totalHT * this.TVA
    }
    this.supprimer = function(p){
        var index = this.ProduitsPanier.findIndex(function(x) {
            return x == p.nom})
            
        // this.removeElement(this.ProduitsPanier,nom);
        this.totalHT = this.totalHT - p.prix;
        this.totalTTC = this.totalTTC -( p.prix * this.TVA)
        console.log(index)
    }
}
// Mon programme

var baguette = new Produit( 'Baguette', 0.85); // prix HT
var croissant = new Produit( 'Croissant', 0.80);

var panier = new Panier();
panier.ajoute(baguette);
panier.ajoute(croissant);
panier.ajoute(croissant);
panier.ajoute(croissant);
panier.supprimer(baguette)

console.log(panier.totalHT);
console.log(panier.totalTTC);   
console.log (panier.ProduitsPanier);
